from django.contrib import admin
from django.urls import include, path
from . import views
urlpatterns = [
    path('', views.ListUser.as_view()),
    path('<int:pk>/', views.DetailUser.as_view()),
]
