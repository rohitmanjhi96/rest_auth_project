from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import generics
# from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from . import models
from . import serializers
from rest_framework.views import APIView
from django.contrib.auth.models import User
from rest_framework import generics, viewsets
# Create your views here.


class CRUDManager(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = models.Manager.objects.all()
    serializer_class = serializers.ManagerSerializer


class UserCRUD(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        usernames = [user.username for user in User.objects.all()]
        serializer_class = serializers.UserSerializer
        return Response(usernames)


class ListUser(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer


class DetailUser(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer


